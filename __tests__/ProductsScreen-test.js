import React from 'react';
import ProductsScreen from '../components/home/ProductsScreen';

import * as renderer from "react-test-renderer";

test('renders correctly', () => {
  const tree = renderer.create(<ProductsScreen />).toJSON();
  expect(tree).toMatchSnapshot();
});