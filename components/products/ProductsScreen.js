import React from "react";
import { StyleSheet, View, Text, Button, ScrollView } from "react-native";
import styles from './ProductsScreen.style.js';

class ProductsScreen extends React.Component {
  static navigationOptions = {
    title: 'Products',
  };

  constructor(props) {
    super(props);
    this.state = { 
      arrayOfProducts: this.props.navigation.state.params.arrayOfProducts,
      chosenAddress: this.props.navigation.state.params.chosenAddress,
      qttOfProducts: this.props.navigation.state.params.qttofProducts,
      totalValue: 0
    };
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <View style={ styles.row }>
          <Text style={ styles.address }>{this.state.chosenAddress}</Text>
          <Button style={ styles.left }
            title="Mudar Endereço"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>
        <ScrollView>
        {
          this.state.arrayOfProducts.map(( item, key ) =>
          (
            <View key = { key } style = { styles.container }>
                <Text style = { styles.text }>{ item.title }</Text>
                <View style = { styles.row }>
                  <Text style = { styles.price }>R$ { item.price }</Text>
                  <View style = {styles.left}>
                    <Button id = { key+'_less' } style = { styles.buttonCard }
                     title="-"
                     onPress={() => {
                        
                        var arrayAux = this.state.qttOfProducts;
                        if(0 < arrayAux[key]){
                          arrayAux[key] = arrayAux[key] - 1;
                          this.setState({ qttOfProducts: arrayAux});
                          var total = parseFloat(this.state.totalValue-item.price);
                          this.setState({totalValue: total});
                        }
                     }}
                    />
                    <Text key = { key+'_text' } style = { styles.qtt }> {this.state.qttOfProducts[key]} </Text>
                    <Button id = { key+'_more' } style = { styles.buttonCard }
                     title="+"
                      onPress={() => {
                        var arrayAux = this.state.qttOfProducts;
                        arrayAux[key] = arrayAux[key] + 1;
                        this.setState({ qttOfProducts: arrayAux});
                        var total = parseFloat(this.state.totalValue+item.price);
                        this.setState({totalValue: total});
                      }}
                    />
                  </View>
                </View>
                <View style = { styles.separator }/>
            </View>
          ))
        }
        </ScrollView>
        <View style={ styles.containerBottom}>
          <Text style={ styles.total}> TOTAL  R$ {Math.abs(this.state.totalValue).toFixed(2)}</Text>
        </View>
      </View>
    );
  }
}

export default ProductsScreen;