import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container:
  {
    flex: 1
  },
  containerBottom:
  {
    marginBottom: 0,
    backgroundColor: 'yellow',
    height: 60,
    width: '100%'
  },
  separator:
  {
    height: 2,
    backgroundColor: 'rgba(0,0,0,0.5)',
    width: '100%'
  },
 
  text:
  {
    fontSize: 18,
    color: 'black',
    padding: 15,
    paddingBottom: 0,
  },
  address:
  {
    fontSize: 15,
    color: 'black',
    padding: 10,
  },
  price:
  {
    fontSize: 24,
    color: 'black',
    paddingTop: 0,
    padding: 15,
    width: 250
  },
  buttonCard:
  {
    padding: 30,
    width: 100
  },
  qtt:
  {
    padding: 15,
    width: 50
  },
  row:
  {
    flexDirection:'row', 
    flexWrap:'wrap'
  },
  left:
  {
    marginLeft: 'auto',
    flexDirection:'row'
  },
  total:
  {
    marginLeft: 'auto',
    fontSize: 28,
    color: 'black',
    padding: 10
  }
});