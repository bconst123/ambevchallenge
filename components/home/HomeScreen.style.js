import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerBottom:
  {
    marginBottom: 0,
    backgroundColor: 'yellow',
    height: 60,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputDataView: {
    width: '100%',
    padding: 10
  },
  typeText: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 10
  },
  button: {
    width: '100%',
    paddingTop: 20,
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  error: {
    fontSize: 15,
    color: 'lightgrey',
    marginTop: 20,
    marginBottom: 20,
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold',
    color: 'green',
    padding: 10,
    marginBottom: 60
  }
});