import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from "graphql-tag";

export const httpLink = createHttpLink({
      uri: 'https://803votn6w7.execute-api.us-west-2.amazonaws.com/dev/public/graphql'
    });

export const client = new ApolloClient({
      link: httpLink,
      cache: new InMemoryCache()
    });

export const GET_PDV = gql`
      query pocSearchMethod($now: DateTime!, $algorithm: String!, $lat: String!, $long: String!) {
      pocSearch(now: $now, algorithm: $algorithm, lat: $lat, long: $long) {
        __typename
        id
        status
        tradingName
        officialName
        deliveryTypes {
          __typename
          pocDeliveryTypeId
          deliveryTypeId
          price
          title
          subtitle
          active
        }
        paymentMethods {
          __typename
          pocPaymentMethodId
          paymentMethodId
          active
          title
          subtitle
        }
        pocWorkDay {
          __typename
          weekDay
          active
          workingInterval {
            __typename
            openingTime
            closingTime
          }
        }
        address {
          __typename
          address1
          address2
          number
          city
          province
          zip
          coordinates
        }
        phone {
          __typename
          phoneNumber
        }
      }
    }
    `;

export 
const GET_PROD = gql`
      query pocCategorySearch($id: ID!, $search: String!, $categoryId: Int!) {
      poc(id: $id) {
        products(categoryId: $categoryId, search: $search) {
          productVariants {
            title
            description
            imageUrl
            price
          }
        }
      }
    }
    `;