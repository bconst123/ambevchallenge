import React from "react";
import { StyleSheet, View, Text, Button, TextInput, ScrollView } from "react-native";
import Geocoder from 'react-native-geocoding';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from "graphql-tag";
import styles from './HomeScreen.style.js';
import * as apiConstants from './HomeScreen.constants.js';


class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  constructor(props) {
    super(props);
    this.state = { 
      text: 'Rua Américo Brasiliense, São Paulo',
      pdvId: 0,
      latitude: "-23.6504603",
      longitude: "-46.6490997",
      arrayOfProducts: [],
      qttofProducts: [],
      ErrorMsg: ""
    };
  }

  changeViewToProducts =() => {
    this.setState({ErrorMsg:"Aguarde enquanto estamos processando ..."})
    Geocoder.init('AIzaSyAvqPdNnDqvimpTIbVtvsAB6Z8pzYEszhk');
    /////Geocoder.from("Rua Américo Brasiliense, São Paulo")
    Geocoder.from(this.state.text)
    .then(json => {
      var location = json.results[0].geometry.location;
    //  console.log(location.lat, location.lng);
      this.setState({latitude:location.lat})
      this.setState({longitude:location.lng})
    })
    .catch(error => console.warn(error));
    

/****************** Retrieving PDV Id and Products********************************/
    const httpLink = apiConstants.httpLink

    const client = apiConstants.client

    const GET_PDV = apiConstants.GET_PDV

    const GET_PROD = apiConstants.GET_PROD

    client.query({
      query: GET_PDV,
      variables: {
        "algorithm": "NEAREST",
        "lat": this.state.latitude,
        "long": this.state.longitude,
        "now": new Date()
      }
    }).then(result => {
        console.log(result);
        this.setState({pdvId:result.data.pocSearch[0].id})//Setting only the first position "0" because I am not showing a choose option in the screen
        if(this.state.pdvId != 0) {
          /*******getProducts*******/
          client.query({
            query: GET_PROD,
            variables: {
              "id": this.state.pdvId,
              "search": "",
              "categoryId": 0
            }
          }).then(result => {
              //console.log(result);
              this.setState({ErrorMsg:""})
              const resultArray = [];
              const qttArray = [];
              for (var key in result.data.poc.products) {
                resultArray.push({
                  key: key,
                  title: result.data.poc.products[key].productVariants[0].title,
                  price: result.data.poc.products[key].productVariants[0].price,
                  imageUrl: result.data.poc.products[key].productVariants[0].imageUrl,
                });
                qttArray.push(0);
              };
              this.state.arrayOfProducts = resultArray;
              this.state.qttofProducts = qttArray;
              if(resultArray.length > 0) {
                this.props.navigation.navigate('Products', {arrayOfProducts: this.state.arrayOfProducts, chosenAddress: this.state.text, qttofProducts: this.state.qttofProducts});
              } else {
                this.setState({ErrorMsg:"Desculpe, Não conseguimos encontrar uma loja perto de você!"})
                console.log("There is no products in this location");
              }
            })
            .catch(err => console.log(err));
          /***************************/
        } else {
          this.setState({ErrorMsg:"Desculpe, Não conseguimos encontrar uma loja perto de você!"})
          console.log("There is no POC near this location");
        }
      })
      .catch(err => console.log(err));

/*********************************************************************/
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
        <View style={styles.inputDataView}>
          <Text style={styles.title}>Olá, Bem Vindo!</Text>
          <Text style={styles.text}>Entre seu endereço:</Text>
          <TextInput
            style={styles.typeText}
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
          />
          <View style={styles.button}>
              <Button
                title="Ver Produtos"
                onPress={this.changeViewToProducts}
              />
          </View>
          <Text style={styles.error}>{this.state.ErrorMsg}</Text>
        </View>  
        </ScrollView>
        <View style={ styles.containerBottom}>
          <Text> ® Bruno Constantino, 2018 </Text>
        </View>
      </View>

    );
  }
}



export default HomeScreen;