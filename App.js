import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from './components/home/HomeScreen';
import ProductsScreen from './components/products/ProductsScreen';


/*************************** Navigation Bar*****************************************************/

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Products: ProductsScreen,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'green',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

const AppContainer = createAppContainer(RootStack);


export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
